package com.zuitt.S11activity.repositories;
import com.zuitt.S11activity.models.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends CrudRepository<User, Object> {

}
