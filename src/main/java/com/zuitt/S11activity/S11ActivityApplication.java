package com.zuitt.S11activity;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class S11ActivityApplication {

	public static void main(String[] args) {
		SpringApplication.run(S11ActivityApplication.class, args);
	}

}

