package com.zuitt.S11activity.services;

import com.zuitt.S11activity.models.Post;
import org.springframework.http.ResponseEntity;

public interface PostService {

    void createPost(Post post);

    Iterable<Post> getPosts();

    ResponseEntity deletePost(Long id);

    ResponseEntity updatePost(Long id, Post post);

}