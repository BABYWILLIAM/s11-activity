package com.zuitt.S11activity.services;

import com.zuitt.S11activity.models.User;
import org.springframework.http.ResponseEntity;

public interface UserService {

    void createUser(User user);

    Iterable<User> getUsers();

    ResponseEntity deleteUser(Long id);

    ResponseEntity updateUser(Long id, User post);

}