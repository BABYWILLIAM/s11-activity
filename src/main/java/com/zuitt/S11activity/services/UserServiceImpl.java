package com.zuitt.S11activity.services;

import com.zuitt.S11activity.models.User;
import com.zuitt.S11activity.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
// The @Service annotation will allow us to use the CRUD Operation/Method inherited from CRUD Repository even though interfaces do not contain implementation/method bodies
public class UserServiceImpl implements UserService {

    //@Autowired indicates that Spring should automatically inject an instance of the PostRepository into the postsRepository
    @Autowired
    //"postRepository" is a variable that represents the instance of PostRepository
    private UserRepository userRepository;

    //Create post
    public void createUser(User user) {
        userRepository.save(user);
    }

    //Get posts
    public Iterable<User> getUsers(){
        return userRepository.findAll();
    }

    //Deleting a post
    public ResponseEntity deleteUser(Long id){
        userRepository.deleteById(id);
        return new ResponseEntity<>("User deleted successfully!", HttpStatus.OK);
    }

    //Update post
    public ResponseEntity updateUser(Long id, User user){
        User userForUpdating = userRepository.findById(id).get();

       userForUpdating.setUsername(user.getUsername());
        userForUpdating.setPassword(user.getPassword());
        userRepository.save(userForUpdating);

        return new ResponseEntity("User updated successfully!", HttpStatus.OK);
    }
}