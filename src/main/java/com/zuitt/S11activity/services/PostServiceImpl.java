package com.zuitt.S11activity.services;

import com.zuitt.S11activity.models.Post;
import com.zuitt.S11activity.repositories.PostRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
// The @Service annotation will allow us to use the CRUD Operation/Method inherited from CRUD Repository even though interfaces do not contain implementation/method bodies
public class PostServiceImpl implements PostService {

    //@Autowired indicates that Spring should automatically inject an instance of the PostRepository into the postsRepository
    @Autowired
    //"postRepository" is a variable that represents the instance of PostRepository
    private PostRepository postRepository;

    //Create post
    public void createPost(Post post) {
        postRepository.save(post);
    }

    //Get posts
    public Iterable<Post> getPosts(){
        return postRepository.findAll();
    }

    //Deleting a post
    public ResponseEntity deletePost(Long id){
        postRepository.deleteById(id);
        return new ResponseEntity<>("Post deleted successfully!", HttpStatus.OK);
    }

    //Update post
    public ResponseEntity updatePost(Long id, Post post){
        Post postForUpdating = postRepository.findById(id).get();

        postForUpdating.setTitle(post.getTitle());
        postForUpdating.setContent(post.getContent());
        postRepository.save(postForUpdating);

        return new ResponseEntity("Post updated successfully!", HttpStatus.OK);
    }
}